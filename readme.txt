Silencesoft jQuery Slide
by Byron Herrera

Description
===========

A jQuery plugin to create slides.

Use
===

* Just create a master div, a content div and for every slide an item div.
Wrapper div is for arrows css position.

Sample:

<div id="wrapper-div" style="position: relative;width:100px;">
<div id="main-div" style="width:100px;height:100px;overflow:hidden;position:relative;">
<div id="content-div">
<div class="slide-div">1</div>
<div class="slide-div">2</div>
<div class="slide-div">3</div>
</div>
</div>
</div>

* Add css to header.

<link rel="stylesheet" type="text/css" href="sil_slide.css">

* Include jquery and slider plugin and load script.

<script type="text/javascript" src="jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="jquery.sil_slide.0.5.js"></script>

* Call it.

    <script type="text/javascript">
    $(document).ready(function() {
        $('#main-div').sil_slide({
			wrapper: '#content-div',
			slide: '.slide-div'
			});
    }); 
    </script>

* And that's all.

In sample.html you can see more options.

Options
=========

Current parametters to use in slider:

wrapper
-------
default value: '#sil_sliderContainer'
Content slider container.

slide
-----
default value: '.sil_slide',
Slider item container. Should be a class.

infinite
--------
default value: false
Allows to create an infinite slider.

autoplay
--------
default value: false
Change slides automatically.


useButtons
----------
default value: true
Show previous and next buttons.

useNavigation
-------------
default value: false
Shows numerated navigation at bottom.

useKeyboard
-----------
default value: false
Allow to use left and right keys on keyboard.
(Experimental, could be buggy).

wait
----
default value: 3000
Autoplay waiting time.

speed
-----
default value: 1000
Slider animation velocity in autoplay.

Changelog
=========

* 0.5
2013-06-22
Created this documentation.
Fixed arrows bug when not infinite.
Fixed autoplay bugs.

* 0.3
2013-03-14
Added options: infinite, autoplay, useButtons, useNavigation, wait, speed
Fixed position bugs.

* 0.1
2012-11-24
First release as jQuery plugin

* First release
Not a plugin, just a function to create slides.

Thanks to
=========

Leonardo Monsalve at once11.com
for current icons.

Contact
=======

http://silencesoft.pw
bh [at] silencesoft [dot] pw

