/*
* Silencesoft jQuery Slide by Byron Herrera
* @Version: 0.5
* @Release: 2013-06-22
* @Page http://silencesoft.pw
* try at http://silencesoft.pw/jQuery

* Permission is hereby granted, free of charge, to any person
* obtaining a copy of this software and associated documentation
* files (the "Software"), to deal in the Software without
* restriction, including without limitation the rights to use,
* copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the
* Software is furnished to do so, subject to the following
* conditions:
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
* OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
* HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
* OTHER DEALINGS IN THE SOFTWARE.
*/


(function($) {
 
    //Attach this new method to jQuery
	$.fn.sil_slide = function(settings) {
		
		main = this;
		
		var defaults = {
			wrapper: '#sil_sliderContainer',
			slide: '.sil_slide',
			infinite: false,
			autoplay: false,
			useButtons: true,
			useNavigation: false,
			useKeyboard: false,
			wait: 3000, // autoplay waiting time
			speed: 1000
		}
		var options = $.extend(defaults, settings);
		options.slide = "#"+main.attr('id')+" "+options.slide;
		if (options.autoplay == true && options.infinite == false)
			options.infinite = true;
		var currentPosition = 0;
		var slideWidth = jQuery(options.slide).width();
		var slideHeight = main.height();
		var slides = jQuery(options.slide);
		var numberOfSlides = slides.length;
		var slideTotalWidth = slideWidth * numberOfSlides;
		var div_inner_id = main.attr('id')+"_inner";
		var div_right_id = main.attr('id')+"_right";
		var div_left_id = main.attr('id')+"_left";
		var div_control_id = main.attr('id')+"_control";
		var div_navigation_id = main.attr('id')+"_navigation";
		var interval = null;

		var managesil_controls = function (position)
		{
			if (options.infinite) return;
			// Hide left arrow if position is first slide
			if (position == 0 || !options.useButtons) { jQuery('#'+div_left_id).hide(); } else{ jQuery('#'+div_left_id).show(); }
			// Hide right arrow if position is last slide
			if(position == numberOfSlides-1  || !options.useButtons) { jQuery('#'+div_right_id).hide(); } else{ jQuery('#'+div_right_id).show(); }
		}
		var autoplay = function()
		{
			jQuery('#'+div_right_id).click();
		}

		jQuery(options.wrapper).css('overflow', 'hidden');
		slides
		.wrapAll('<div id="'+div_inner_id+'"></div>')
		.css({
			'float' : 'left' ,
			'width' : slideWidth
		});
		if (numberOfSlides > 1)
		{
			jQuery('#'+div_inner_id).css({'left' : 0});
			leftPosition = 0;
	//		jQuery(options.slide+":first").before(jQuery(options.slide+":last"));
	//		jQuery('#'+div_inner_id).css({'left' : slideWidth*-1});
		}
		jQuery('#'+div_inner_id).css({'width': slideTotalWidth, 'position' : 'relative'});

		main
		.before('<span class="sil_control '+div_control_id+' sil_leftControl" id="'+div_left_id+'">Clicking moves left</span>')
		.after('<span class="sil_control '+div_control_id+' sil_rightControl" id="'+div_right_id+'">Clicking moves right</span>');
		jQuery('#'+div_left_id).css('height', slideHeight);
		jQuery('#'+div_right_id).css('height', slideHeight);
		managesil_controls(currentPosition);
		if (!options.useButtons)
		{
			jQuery('#'+div_left_id).hide();
			jQuery('#'+div_right_id).hide();
		}

		if (options.useNavigation)
		{
			main
			.after('<div id="'+div_navigation_id+'" class="sil_navigation"><ul></ul></div>');
			var list = jQuery('#'+div_navigation_id+' ul');
			if (numberOfSlides > 1)
			{
				for ( var i = 1; i < numberOfSlides+1; i++ ) {
				    list.append( '<li>' + i + '</li>' );
				}
			}
			items = jQuery('#'+div_navigation_id+' ul li:first').addClass('sil_navigation_current');
			var items = jQuery('#'+div_navigation_id+' ul li');
			items.bind('click', function(){
				items.removeClass('sil_navigation_current');
				jQuery(this).addClass('sil_navigation_current');
				var val = parseInt(jQuery(this).text())-1;
				var position = slideWidth*val*-1;
				jQuery('#'+div_inner_id).animate({
					'left' : position
					}, options.speed,function(){
					});
			});
		}

		if (options.autoplay)
		{
            interval = setInterval( autoplay, options.wait);   

			jQuery(options.wrapper)
			.hover(
				function() {
					clearInterval(interval);
				}, 
				function() {
					interval = setInterval(autoplay, options.wait);   
				}
			);
		}

		if (options.useKeyboard)
		{
			$(document.documentElement).keyup(function (event) {
				if (event.keyCode == 37)
				{
					jQuery('#'+div_left_id).click();
				}
				else if (event.keyCode == 39)
				{
					jQuery('#'+div_right_id).click();
				}
			});
		}

		jQuery('.'+div_control_id)
		// .css('height', slideHeight)
		.bind('click', function(){
			if (numberOfSlides == 1) return;
			var goNext = (jQuery(this).attr('id')==div_right_id) ? true : false;
			var leftPosition = parseInt(jQuery('#'+div_inner_id).css('left'));
			if (leftPosition%slideWidth) return;
			var done = false;
			if (goNext)
			{
				if (!options.infinite && currentPosition >= numberOfSlides-1) return;
				if (options.infinite)
				if ((leftPosition - slideWidth) + slideTotalWidth == 0)
				{
						jQuery(options.slide+":last").after(jQuery(options.slide+":first"));
						leftPosition += slideWidth;
						jQuery('#'+div_inner_id).css({'left' : leftPosition});
						done = true;
				}
				leftPosition -= slideWidth;
				jQuery('#'+div_inner_id).animate({
					'left' : leftPosition
					}, options.speed,function(){
						if (!done)
						{
						}
					}
					);
				currentPosition++;
			}
			else
			{
				if (!options.infinite && currentPosition <= 0) return;
				if (options.infinite)
				if (leftPosition == 0)
				{
						jQuery(options.slide+":first").before(jQuery(options.slide+":last"));
						jQuery('#'+div_inner_id).css({'left' : leftPosition-slideWidth});
						leftPosition -= slideWidth;
						done = true;
				}
				leftPosition += slideWidth;
				jQuery('#'+div_inner_id).animate({
					'left' : leftPosition
					}, options.speed, function(){
						if (!done)
						{
						}
					});
				currentPosition--;
			}
			managesil_controls(currentPosition);
		});
    }
})(jQuery);
